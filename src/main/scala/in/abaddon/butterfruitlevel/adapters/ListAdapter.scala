package in.abaddon.butterfruitlevel.adapters

import java.util.Optional

import android.content.Context
import android.graphics.Color
import android.text.format.DateFormat
import android.view.{LayoutInflater, View, ViewGroup}
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import in.abaddon.butterfruitlevel.R
import in.abaddon.butterfruitlevel.models._
import in.abaddon.butterfruitlevel.utils.{WithDateTime, WithFormatter}

import scala.collection.mutable.ListBuffer

trait OnItemClickListener {
  def onItemClick(view: View, position: Int)
}

case class ViewHolder(rowView: View,
                      glucose: TextView,
                      ketone: TextView,
                      gki: TextView,
                      timestamp: TextView,
                      indicator: View)
    extends RecyclerView.ViewHolder(rowView)

object ViewHolder {
  def apply(rowView: View): ViewHolder = {
    val glucose: TextView = rowView.findViewById(R.id.row_glucose_label)
    val ketone: TextView = rowView.findViewById(R.id.row_ketone_label)
    val gki: TextView = rowView.findViewById(R.id.row_gki_label)
    val timestamp: TextView = rowView.findViewById(R.id.row_timestamp)
    val indicator: View = rowView.findViewById(R.id.row_indicator)

    ViewHolder(rowView, glucose, ketone, gki, timestamp, indicator)
  }
}

class ListAdapter(data: ListBuffer[Reading],
                  onItemClickListener: OnItemClickListener,
                  selectedColor: Int,
                  ctx: Context)
    extends RecyclerView.Adapter[ViewHolder]
    with WithDateTime
    with WithFormatter
    with WithLevelUtils {

  private var selectedPos = -1

  private lazy val prefGlucose = getPrefGlucose
  private lazy val prefKetone = getPrefKetone

  private val GENERAL_PREF = "GENERAL_PREF"

  private val PREF_GLUCOSE_UNIT = "PREF_GLUCOSE_UNIT"
  private val PREF_KETONE_UNIT = "PREF_KETONE_UNIT"

  private def getSharedPref =
    ctx.getSharedPreferences(GENERAL_PREF, Context.MODE_PRIVATE)
  private def getPrefGlucose = getSharedPref.getInt(PREF_GLUCOSE_UNIT, 0)
  private def getPrefKetone = getSharedPref.getInt(PREF_KETONE_UNIT, 0)

  override def onCreateViewHolder(parent: ViewGroup, viewType: Int) = {
    val rowView = LayoutInflater
      .from(parent.getContext)
      .inflate(R.layout.row_item, parent, false)
    val vh = ViewHolder(rowView)

    rowView.setOnClickListener(new View.OnClickListener {
      override def onClick(v: View): Unit = {
        selectItem(vh.getAdapterPosition)
        onItemClickListener.onItemClick(v, selectedPos)
      }
    })

    vh
  }

  override def onBindViewHolder(vh: ViewHolder, pos: Int) {
    val reading = data(pos)

    vh.glucose.setText(
      formatDecimalWithUnit(reading.getGlucose, getUnit(prefGlucose), Glucose))

    vh.ketone.setText(
      formatDecimalWithUnit(reading.getKetone, getUnit(prefKetone), Ketone))

    vh.gki.setText(formatDecimal(reading.getGKI))

    val formattedDate = formatDate(
      "dd-MM-yy hh:mm",
      reading.getTimestamp,
      DateFormat.is24HourFormat(vh.glucose.getContext))
    vh.timestamp.setText(formattedDate)

    val color = if (selectedPos == pos) selectedColor else Color.TRANSPARENT
    vh.indicator.setBackgroundColor(color)
  }

  private def formatDecimalWithUnit(value: Optional[Float],
                                    unit: Dimension,
                                    rtype: ReadingType): String = {
    if (unit == MMOL) {
      return formatDecimal(value)
    }

    if (value.isPresent) formatDecimal(convertToMG(value.get, rtype))
    else ""
  }

  private def getUnit(pref: Int) = pref match {
    case 0 => MMOL
    case 1 => MG
    case _ => throw new IllegalArgumentException("unexpected position")
  }

  def selectItem(pos: Int) {
    val prevPos = selectedPos
    selectedPos = if (selectedPos != pos) pos else -1

    notifyItemChanged(prevPos)
    notifyItemChanged(selectedPos)
  }

  override def getItemCount =
    data.size

}
