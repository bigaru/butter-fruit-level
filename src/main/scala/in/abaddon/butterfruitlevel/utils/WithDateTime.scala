package in.abaddon.butterfruitlevel.utils

import java.util.Locale

import android.text.format.DateFormat
import org.joda.time.{DateTime, MutableDateTime}
import org.joda.time.format.DateTimeFormat

trait WithDateTime {
  val DAY_IN_SECONDS = 86400

  def formatDate(template: String,
                 dateTimeInSeconds: Int,
                 is24Format: Boolean): String = {
    val hourChar = if (is24Format) "H" else "h"
    val updatedTemplate = template.replaceAll("[hHj]", hourChar)
    val pattern =
      DateFormat.getBestDateTimePattern(Locale.getDefault(), updatedTemplate)

    val dtFormatter =
      DateTimeFormat.forPattern(pattern).withLocale(Locale.getDefault())
    dtFormatter.print(fromSeconds(dateTimeInSeconds))
  }

  def formatDate(template: String,
                 dateTimeInSeconds: Float,
                 is24Format: Boolean): String =
    formatDate(template, dateTimeInSeconds.intValue(), is24Format)

  def fromSeconds(timestamp: Int) =
    new DateTime(timestamp * 1000L)

  def toSeconds(dateTime: DateTime): Int =
    (dateTime.getMillis / 1000).asInstanceOf[Int]

  def toSeconds(dateTime: MutableDateTime): Int =
    (dateTime.getMillis / 1000).asInstanceOf[Int]

  def getCalenderMonth(monthOfYear: Int) =
    if (monthOfYear > 12 || monthOfYear < 1)
      throw new IllegalArgumentException("expected [1-12]")
    else monthOfYear - 1

  def getMonthOfYear(calendarMonth: Int) =
    if (calendarMonth > 11 || calendarMonth < 0)
      throw new IllegalArgumentException("expected [0-11]")
    else calendarMonth + 1

}
