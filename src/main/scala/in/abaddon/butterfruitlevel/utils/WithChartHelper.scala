package in.abaddon.butterfruitlevel.utils

import android.graphics.Color

import com.github.mikephil.charting.data.LineDataSet

trait WithChartHelper extends WithDateTime {
  val GRID_WIDTH = 0.5f

  val Y_INTERVAL = 0.5f
  val X_INTERVAL = DAY_IN_SECONDS

  val CIRCLE_RADIUS = 2.5f
  val LINE_WIDTH = 1.75f

  def styleLineDataSet(dataSet: LineDataSet, color: Int, circleColor: Int) {
    dataSet.setDrawCircleHole(false)
    dataSet.setDrawCircles(true)
    dataSet.setColor(color)

    dataSet.setCircleRadius(CIRCLE_RADIUS)
    dataSet.setCircleColor(circleColor)
    dataSet.setLineWidth(LINE_WIDTH)
    dataSet.setDrawValues(false)

    dataSet.setDrawHorizontalHighlightIndicator(false)
    dataSet.setDrawVerticalHighlightIndicator(true)
    dataSet.setHighLightColor(Color.BLACK)
    dataSet.setHighlightLineWidth(1.2f)

    dataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER)
    dataSet.setDrawIcons(false)

    dataSet.setDrawFilled(true)
    dataSet.setFillColor(color)
    dataSet.setFillAlpha(255)
  }
}
