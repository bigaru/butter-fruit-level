package in.abaddon.butterfruitlevel.utils

import android.text.Editable
import android.text.TextWatcher

trait OnTextChangedListener {
  def fieldTextChanged(s: Editable, textEditId: Int): Unit
}

class ReadingTextWatcher(id: Int, onTextChangedListener: OnTextChangedListener)
    extends TextWatcher {

  override def beforeTextChanged(s: CharSequence,
                                 start: Int,
                                 count: Int,
                                 after: Int): Unit = {}
  override def onTextChanged(s: CharSequence,
                             start: Int,
                             before: Int,
                             count: Int): Unit = {}
  override def afterTextChanged(s: Editable): Unit = {
    onTextChangedListener.fieldTextChanged(s, id)
  }
}
