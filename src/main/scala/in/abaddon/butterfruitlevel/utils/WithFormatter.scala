package in.abaddon.butterfruitlevel.utils

import java.text.DecimalFormat
import java.util.Optional

trait WithFormatter {
  def formatDecimal(optVal: Optional[Float]): String = {
    if (optVal.isPresent) formatDecimal(optVal.get)
    else ""
  }

  def formatDecimal(value: Float): String = {
    val df2 = new DecimalFormat("0.00")
    df2.format(value)
  }
}
