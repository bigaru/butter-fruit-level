package in.abaddon.butterfruitlevel.views

import android.content.Context
import android.util.AttributeSet

import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.formatter.ValueFormatter

import in.abaddon.butterfruitlevel.R
import in.abaddon.butterfruitlevel.utils.WithChartHelper

class SupremeLineChart(context: Context, attrs: AttributeSet, defStyle: Int)
    extends LineChart(context, attrs, defStyle)
    with WithChartHelper {

  def this(context: Context, attrs: AttributeSet) {
    this(context, attrs, 0)
    defaultInit(context, attrs)
  }

  def this(context: Context) {
    this(context, null, 0)
    defaultInit(context, attrs)
  }

  private def defaultInit(context: Context, attributeSet: AttributeSet) {
    val GRID_COLOR = context.getColor(R.color.grey400)

    mAxisRight.setEnabled(false)

    mXAxis.setValueFormatter(new ValueFormatter() {
      override def getFormattedValue(value: Float) =
        formatDate("dd.MM", value, is24Format = false)
    })

    mXAxis.setDrawAxisLine(false)
    mXAxis.setDrawGridLines(false)
    mXAxis.setGranularity(X_INTERVAL)

    mAxisLeft.setDrawAxisLine(false)
    mAxisLeft.setGranularity(Y_INTERVAL)
    mAxisLeft.setGridColor(GRID_COLOR)
    mAxisLeft.setGridLineWidth(GRID_WIDTH)
    mAxisLeft.setAxisMaximum(10)
    mAxisLeft.setAxisMinimum(0)

    mDescription.setEnabled(false)
    setDrawBorders(true)
    setBorderColor(GRID_COLOR)
    setBorderWidth(GRID_WIDTH)

    setPinchZoom(false)
    setDoubleTapToZoomEnabled(false)
  }
}
