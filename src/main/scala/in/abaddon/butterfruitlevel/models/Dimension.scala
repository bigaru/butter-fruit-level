package in.abaddon.butterfruitlevel.models

sealed trait Dimension
case object MMOL extends Dimension
case object MG extends Dimension
