package in.abaddon.butterfruitlevel.models

import java.io.{FileOutputStream, ObjectOutputStream}
import java.util.{Optional, UUID}

import android.content.Context
import in.abaddon.butterfruitlevel.DefaultApplication

import scala.collection.mutable.ListBuffer

class MemoryReadingRepository(context: Context) extends ReadingDao {
  private val readings: ListBuffer[Reading] = ListBuffer()

  private def getPredicate(rtype: ReadingType) = rtype match {
    case Glucose =>
      (reading: Reading) =>
        reading.getGlucose.isPresent
    case Ketone =>
      (reading: Reading) =>
        reading.getKetone.isPresent
    case GKI =>
      (reading: Reading) =>
        reading.getGlucose.isPresent && reading.getKetone.isPresent
    case AnyReading =>
      (reading: Reading) =>
        reading.getGlucose.isPresent || reading.getKetone.isPresent

    case _ => throw new AssertionError("Case not implemented")
  }

  def getMany(rtype: ReadingType, beginAt: Int, count: Int): List[Reading] =
    readings
      .filter(getPredicate(rtype))
      .sortBy(_.timestamp)
      .reverse
      .slice(beginAt, beginAt + count)
      .toList

  def get(id: UUID): Optional[Reading] =
    readings.find(r => r.id == id) match {
      case Some(v) => Optional.of(v)
      case _       => Optional.empty()
    }

  def add(newReading: Reading) {

    if (get(newReading.getId).isPresent) {
      throw new IllegalArgumentException("Reading with same Id already exists")
    }

    readings.append(newReading)
  }

  def update(id: UUID, updatedReading: Reading): Boolean = {
    val found = get(id)
    if (!found.isPresent) return false

    val pos = readings.indexOf(found.get)
    readings.update(pos, updatedReading)

    true
  }

  def delete(id: UUID): Boolean = {
    val found = get(id)
    if (!found.isPresent) return false

    val pos = readings.indexOf(found.get)
    readings.remove(pos)

    true
  }

  def size(rtype: ReadingType): Long =
    readings.count(getPredicate(rtype))

  def persistData() {
    var fos: FileOutputStream = null

    try {
      fos = context.openFileOutput(DefaultApplication.DATA_FILE_NAME,
                                   Context.MODE_PRIVATE)
      val oos = new ObjectOutputStream(fos)
      oos.writeObject(readings)
      oos.close()
    } catch {
      case e: Throwable => e.printStackTrace()
    }
  }
}
