package in.abaddon.butterfruitlevel.models

trait WithLevelUtils {

  /*
   * Conversions ********************************
   */

  /*
   * Glucose
   * conversion factor: 1 mmol/L = 18.018 mg/dL
   */
  val CF_GLUCOSE_MMOL_MG = 18.018f

  /*
   * Beta-hydroxybutyrate ketone
   * conversion factor: 1 mmol/L = 10.4 mg/dL
   */
  val CF_KETONE_MMOL_MG = 10.4f

  def glucose_fromMMOL_toMG(value: Float) =
    value * CF_GLUCOSE_MMOL_MG

  def glucose_fromMG_toMMOL(value: Float) =
    value / CF_GLUCOSE_MMOL_MG

  def ketone_fromMMOL_toMG(value: Float) =
    value * CF_KETONE_MMOL_MG

  def ketone_fromMG_toMMOL(value: Float) =
    value / CF_KETONE_MMOL_MG

  def convertToMMOL(value: Float, readingType: ReadingType) =
    readingType match {
      case Glucose => glucose_fromMG_toMMOL(value)

      case Ketone => ketone_fromMG_toMMOL(value)

      case _ =>
        throw new IllegalArgumentException(
          "ReadingType must be Glucose or Ketone")
    }

  def convertToMG(value: Float, readingType: ReadingType) = readingType match {
    case Glucose => glucose_fromMMOL_toMG(value)

    case Ketone => ketone_fromMMOL_toMG(value)

    case _ =>
      throw new IllegalArgumentException(
        "ReadingType must be Glucose or Ketone")
  }

  /*
   * Glucose Validity ********************************
   */

  val MAX_GLUCOSE_MG = 200
  val MIN_GLUCOSE_MG = 50

  def isGlucoseValid_MG(value: Float) =
    MIN_GLUCOSE_MG <= value && value < MAX_GLUCOSE_MG

  def isGlucoseValid_MMOL(value: Float) = {
    val max = glucose_fromMG_toMMOL(MAX_GLUCOSE_MG)
    val min = glucose_fromMG_toMMOL(MIN_GLUCOSE_MG)

    min <= value && value < max
  }

  /*
   * Ketone Validity ********************************
   */

  val MAX_KETONE_MMOL = 10.5f
  val MIN_KETONE_MMOL = 0

  def isKetoneValid_MMOL(value: Float) =
    MIN_KETONE_MMOL <= value && value < MAX_KETONE_MMOL

  def isKetoneValid_MG(value: Float) = {
    val max = ketone_fromMMOL_toMG(MAX_KETONE_MMOL)
    val min = ketone_fromMMOL_toMG(MIN_KETONE_MMOL)

    min <= value && value < max
  }

  /*
   * Validity ********************************
   */

  def isReadingValid(readingType: ReadingType,
                     dimension: Dimension,
                     value: Float) = readingType match {
    case Glucose =>
      dimension match {
        case MMOL => isGlucoseValid_MMOL(value)
        case MG   => isGlucoseValid_MG(value)
      }

    case Ketone =>
      dimension match {
        case MMOL => isKetoneValid_MMOL(value)
        case MG   => isKetoneValid_MG(value)
      }

    case _ => false
  }

}
