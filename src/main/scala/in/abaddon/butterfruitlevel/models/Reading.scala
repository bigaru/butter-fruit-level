package in.abaddon.butterfruitlevel.models

import java.util.{Optional, UUID}

@SerialVersionUID(100L)
case class Reading(id: UUID,
                   timestamp: Int,
                   glucose: Option[Float],
                   ketone: Option[Float],
                   notes: String)
    extends Serializable {
  def getId = id
  def getTimestamp = timestamp
  def getGlucose: Optional[Float] =
    if (glucose.isDefined) Optional.of(glucose.get) else Optional.empty()
  def getKetone: Optional[Float] =
    if (ketone.isDefined) Optional.of(ketone.get) else Optional.empty()
  def getGKI: Optional[Float] =
    if (glucose.isDefined && ketone.isDefined) {
      Optional.of(glucose.get / ketone.get)
    } else Optional.empty()
  def getNotes = notes
}

object Reading {
  def apply(timestamp: Int,
            glucose: Option[Float],
            ketone: Option[Float],
            notes: String): Reading =
    Reading(UUID.randomUUID(), timestamp, glucose, ketone, notes)
}
