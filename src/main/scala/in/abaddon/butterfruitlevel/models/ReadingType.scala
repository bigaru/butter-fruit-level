package in.abaddon.butterfruitlevel.models

sealed trait ReadingType
case object Glucose extends ReadingType
case object Ketone extends ReadingType
case object GKI extends ReadingType
case object AnyReading extends ReadingType
