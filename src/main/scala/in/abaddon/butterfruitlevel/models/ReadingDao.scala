package in.abaddon.butterfruitlevel.models

import java.util.Optional
import java.util.UUID

trait ReadingDao {

  /**
    * get sorted by timestamp desc
    */
  def getMany(rtype: ReadingType, beginAt: Int, count: Int): List[Reading]
  def get(id: UUID): Optional[Reading]
  def add(newReading: Reading)
  def update(id: UUID, updatedReading: Reading): Boolean
  def delete(id: UUID): Boolean
  def size(rtype: ReadingType): Long
  def persistData()
}
