package in.abaddon.butterfruitlevel

import android.app.Application
import in.abaddon.butterfruitlevel.adapters.{ListAdapter, OnItemClickListener}
import in.abaddon.butterfruitlevel.models.{
  MemoryReadingRepository,
  Reading,
  ReadingDao
}

import scala.collection.mutable.ListBuffer

class DefaultApplication extends Application {
  protected var readingDao: ReadingDao = _

  def getReadingDao: ReadingDao = {
    if (readingDao == null)
      readingDao = new MemoryReadingRepository(getApplicationContext)

    readingDao
  }

  def makeListAdapter(data: ListBuffer[Reading],
                      listener: OnItemClickListener) =
    new ListAdapter(data, listener, getColor(R.color.darkGreen), this)

}

object DefaultApplication {
  val DATA_FILE_NAME = "stored_data.bin"
}
