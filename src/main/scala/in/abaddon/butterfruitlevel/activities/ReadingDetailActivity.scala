package in.abaddon.butterfruitlevel.activities

import android.app.{Activity, DatePickerDialog, TimePickerDialog}
import android.content.{Context, SharedPreferences}
import android.os.Bundle
import android.text.Editable
import android.text.format.DateFormat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget._
import com.google.android.material.textfield.TextInputLayout
import org.joda.time.MutableDateTime
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import in.abaddon.butterfruitlevel.DefaultApplication
import in.abaddon.butterfruitlevel.R
import in.abaddon.butterfruitlevel.models._
import in.abaddon.butterfruitlevel.utils.{
  OnTextChangedListener,
  ReadingTextWatcher,
  WithDateTime,
  WithFormatter
}

class ReadingDetailActivity
    extends AppCompatActivity
    with WithDateTime
    with WithLevelUtils
    with WithFormatter
    with View.OnClickListener
    with DatePickerDialog.OnDateSetListener
    with TimePickerDialog.OnTimeSetListener
    with OnTextChangedListener
    with AdapterView.OnItemSelectedListener {

  private val GENERAL_PREF = "GENERAL_PREF"

  private val PREF_GLUCOSE_UNIT = "PREF_GLUCOSE_UNIT"
  private val PREF_KETONE_UNIT = "PREF_KETONE_UNIT"

  private val dateTime = new MutableDateTime()

  private var datePickerDialog: DatePickerDialog = _
  private var timePickerDialog: TimePickerDialog = _
  private var dateView, timeView: TextView = _

  private var glucoseUnit, ketoneUnit: Spinner = _
  private var glucoseWrapper, ketoneWrapper: TextInputLayout = _
  private var glucoseField, ketoneField, gkiField, notesField: EditText = _

  private var gkiOuterLayout: View = _
  private var readingDao: ReadingDao = _
  private var sharedPref: SharedPreferences = _

  override protected def onPause() {
    super.onPause()

    val editor = sharedPref.edit()
    editor.putInt(PREF_GLUCOSE_UNIT, glucoseUnit.getSelectedItemPosition)
    editor.putInt(PREF_KETONE_UNIT, ketoneUnit.getSelectedItemPosition)
    editor.commit()
  }

  override protected def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_reading_detail)
    sharedPref = getSharedPreferences(GENERAL_PREF, Context.MODE_PRIVATE)

    setupActionbar()
    setupUnitAdapters()
    setupDateTimeViews()
    setupEditTexts()

    val app = getApplication.asInstanceOf[DefaultApplication]
    readingDao = app.getReadingDao
    gkiOuterLayout = findViewById(R.id.outer_layout_gki)
  }

  private def setupActionbar() {
    val actionBar = getSupportActionBar

    actionBar.setDisplayHomeAsUpEnabled(true)
    actionBar.setHomeAsUpIndicator(R.drawable.ic_close)

    setActionbarTitle(actionBar)
  }

  private def setupUnitAdapters() {
    glucoseUnit = findViewById(R.id.sp_glucose_unit)
    ketoneUnit = findViewById(R.id.sp_ketone_unit)

    val unitAdapter = ArrayAdapter.createFromResource(
      this,
      R.array.units,
      android.R.layout.simple_spinner_item
    )
    unitAdapter.setDropDownViewResource(
      android.R.layout.simple_spinner_dropdown_item
    )

    glucoseUnit.setAdapter(unitAdapter)
    glucoseUnit.setOnItemSelectedListener(this)

    val prefGlucosePos = sharedPref.getInt(PREF_GLUCOSE_UNIT, 0)
    glucoseUnit.setSelection(prefGlucosePos)

    ketoneUnit.setAdapter(unitAdapter)
    ketoneUnit.setOnItemSelectedListener(this)

    val prefKetonePos = sharedPref.getInt(PREF_KETONE_UNIT, 0)
    ketoneUnit.setSelection(prefKetonePos)
  }

  private def setupDateTimeViews() {
    dateView = findViewById(R.id.picker_label_date)
    dateView.setOnClickListener(this)
    updateDate()

    timeView = findViewById(R.id.picker_label_time)
    timeView.setOnClickListener(this)
    updateTime()

    datePickerDialog = new DatePickerDialog(
      this,
      this,
      dateTime.getYear,
      getCalenderMonth(dateTime.getMonthOfYear),
      dateTime.getDayOfMonth
    )
    timePickerDialog = new TimePickerDialog(
      this,
      this,
      dateTime.getHourOfDay,
      dateTime.getMinuteOfHour,
      DateFormat.is24HourFormat(this)
    )
  }

  private def setupEditTexts() {
    glucoseWrapper = findViewById(R.id.wrapper_glucose)
    ketoneWrapper = findViewById(R.id.wrapper_ketone)

    glucoseField = findViewById(R.id.et_glucose)
    ketoneField = findViewById(R.id.et_ketone)
    gkiField = findViewById(R.id.et_gki)
    notesField = findViewById(R.id.et_notes)

    glucoseField.addTextChangedListener(
      new ReadingTextWatcher(R.id.et_glucose, this)
    )
    ketoneField.addTextChangedListener(
      new ReadingTextWatcher(R.id.et_ketone, this)
    )
  }

  private def setActionbarTitle(actionBar: ActionBar) {
    val title = getIntent.getAction match {
      case MainActivity.ACTION_ADD_READING =>
        getString(R.string.activity_add_reading)
      case _ => getString(R.string.app_name)
    }

    actionBar.setTitle(title)
  }

  private def updateDate() {
    dateView.setText(
      formatDate(
        "EE, dd-MMM-yyyy",
        toSeconds(dateTime),
        DateFormat.is24HourFormat(this)
      )
    )
  }

  private def updateTime() {
    timeView.setText(
      formatDate(
        "hh:mm",
        toSeconds(dateTime),
        DateFormat.is24HourFormat(this)
      )
    )
  }

  override def onOptionsItemSelected(item: MenuItem) = {
    item.getItemId match {
      case android.R.id.home        => finish()
      case R.id.reading_detail_tick => addReading()
    }

    true
  }

  override def onClick(v: View) {
    v.getId match {
      case R.id.picker_label_date => datePickerDialog.show()
      case R.id.picker_label_time => timePickerDialog.show()
    }
  }

  override def onDateSet(view: DatePicker,
                         year: Int,
                         month: Int,
                         dayOfMonth: Int) {
    dateTime.setDate(year, getMonthOfYear(month), dayOfMonth)
    updateDate()
  }

  override def onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
    dateTime.setTime(hourOfDay, minute, 0, 0)
    updateTime()
  }

  override def onCreateOptionsMenu(menu: Menu) = {
    getMenuInflater.inflate(R.menu.menu_reading_detail, menu)
    true
  }

  private def getUnit(position: Int) = position match {
    case 0 => MMOL
    case 1 => MG
    case _ => throw new IllegalArgumentException("unexpected position")

  }

  override def fieldTextChanged(editable: Editable, textEditId: Int) {
    textEditId match {
      case R.id.et_glucose =>
        fieldChanged(
          editable,
          glucoseWrapper,
          glucoseUnit,
          Glucose
        )

      case R.id.et_ketone =>
        fieldChanged(editable, ketoneWrapper, ketoneUnit, Ketone)

    }

    showGKI()
  }

  private def fieldChanged(editable: Editable,
                           wrapper: TextInputLayout,
                           spinner: Spinner,
                           rtype: ReadingType) {
    if (editable.length() == 0) {
      wrapper.setError(null)
      return
    }

    val unit = getUnit(spinner.getSelectedItemPosition)
    val value = editable.toString.toFloat

    if (isReadingValid(rtype, unit, value)) {
      wrapper.setError(null)
      return
    }

    val alternativePos = (spinner.getSelectedItemPosition + 1) % 2
    val alternativeUnit = getUnit(alternativePos)
    val alternativeUnitText =
      spinner.getAdapter.getItem(alternativePos).toString

    if (isReadingValid(rtype, alternativeUnit, value)) {
      wrapper.setError(
        getString(
          R.string.error_invalid_reading_with_proposal,
          alternativeUnitText
        )
      )
      return
    }

    wrapper.setError(getString(R.string.error_invalid_reading))
  }

  private def hasNoError =
    glucoseWrapper.getError == null && ketoneWrapper.getError == null

  private def hasGlucoseVal =
    glucoseField.getText.length() > 0

  private def hasKetoneVal =
    ketoneField.getText.length() > 0

  private def getMMOLValue(spinner: Spinner, value: Float, rtype: ReadingType) =
    if (spinner.getSelectedItemPosition == 0) value
    else convertToMMOL(value, rtype)

  private def getGlucoseMMOLVal: Option[Float] =
    if (!hasGlucoseVal) None
    else {
      val glucoseVal = glucoseField.getText.toString.toFloat
      Some(getMMOLValue(glucoseUnit, glucoseVal, Glucose))
    }

  private def getKetoneMMOLVal: Option[Float] =
    if (!hasKetoneVal) None
    else {
      val ketoneVal = ketoneField.getText.toString.toFloat
      Some(getMMOLValue(ketoneUnit, ketoneVal, Ketone))
    }

  private def showGKI() {
    if (hasNoError && hasGlucoseVal && hasKetoneVal) {
      gkiOuterLayout.setVisibility(View.VISIBLE)

      val glucoseVal = getGlucoseMMOLVal.get
      val ketoneVal = getKetoneMMOLVal.get

      val gkiVal = if (ketoneVal == 0) 0 else glucoseVal / ketoneVal
      gkiField.setText(formatDecimal(gkiVal))

    } else {
      gkiField.setText("-")
    }
  }

  private def addReading() {
    val hasAtLeastOneValue = hasGlucoseVal || hasKetoneVal

    if (!hasAtLeastOneValue) {
      Toast
        .makeText(
          this,
          R.string.error_missing_at_least_value,
          Toast.LENGTH_LONG
        )
        .show()
    }

    if (!hasNoError) {
      Toast
        .makeText(this, R.string.error_contains_invalid, Toast.LENGTH_LONG)
        .show()
    }

    if (hasAtLeastOneValue && hasNoError) {
      val reading = Reading(
        toSeconds(dateTime),
        getGlucoseMMOLVal,
        getKetoneMMOLVal,
        notesField.getText.toString
      )
      readingDao.add(reading)
      setResult(Activity.RESULT_OK)
      finish()
    }

  }

  override def onItemSelected(parent: AdapterView[_],
                              view: View,
                              position: Int,
                              id: Long): Unit = {
    parent.getId match {
      case R.id.sp_glucose_unit =>
        fieldTextChanged(glucoseField.getText, R.id.et_glucose)

      case R.id.sp_ketone_unit =>
        fieldTextChanged(ketoneField.getText, R.id.et_ketone)
    }

  }

  override def onNothingSelected(parent: AdapterView[_]): Unit = {}

}
