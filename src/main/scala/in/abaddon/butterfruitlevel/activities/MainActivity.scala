package in.abaddon.butterfruitlevel.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Pair
import android.view.{Menu, MenuItem, View}
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.{LinearLayoutManager, RecyclerView}
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.{Entry, LineData, LineDataSet}
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.google.android.material.floatingactionbutton.FloatingActionButton
import in.abaddon.butterfruitlevel.adapters.{ListAdapter, OnItemClickListener}
import in.abaddon.butterfruitlevel.models.{AnyReading, Reading, ReadingDao}
import in.abaddon.butterfruitlevel.utils.WithChartHelper
import in.abaddon.butterfruitlevel.views.SupremeLineChart
import in.abaddon.butterfruitlevel.{DefaultApplication, R}

import scala.collection.JavaConverters._
import scala.collection.mutable

class MainActivity
    extends AppCompatActivity
    with WithChartHelper
    with OnItemClickListener
    with OnChartValueSelectedListener
    with View.OnClickListener {

  private val READING_DETAIL_REQUEST = 3

  private val READING_TYPE = AnyReading
  private val PAGE_COUNT = 50

  private var chart: SupremeLineChart = _
  private var fab: FloatingActionButton = _

  private var listAdapter: ListAdapter = _
  private var readingDao: ReadingDao = _
  private var deleteMenu: MenuItem = _

  private val readings = mutable.ListBuffer[Reading]()
  private val layoutManager = new LinearLayoutManager(this)
  private var selectedItem = -1

  private val onScrollListener = new RecyclerView.OnScrollListener() {
    override def onScrolled(rv: RecyclerView, dx: Int, dy: Int) {
      super.onScrolled(rv, dx, dy)

      val isScrollingDown = dy > 0
      val isScrollingUp = dy < 0

      // TODO  see --> https://ayusch.com/android-paginated-recyclerview-with-progress-bar/
      val isLastItemVisible = layoutManager
        .findLastCompletelyVisibleItemPosition() == readings.size - 1
      val hasMore = readings.size < readingDao.size(READING_TYPE)

      if (isScrollingDown && isLastItemVisible && hasMore) {
        addMoreData(false)
      }

      if (isScrollingDown) fab.hide()
      else if (isScrollingUp) fab.show()
    }
  }

  override protected def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    val app = getApplication.asInstanceOf[DefaultApplication]
    readingDao = app.getReadingDao

    fab = findViewById(R.id.fab)
    fab.setOnClickListener(this)

    val recyclerView: RecyclerView = findViewById(R.id.main_recycler_view)
    recyclerView.setHasFixedSize(true)
    recyclerView.setLayoutManager(layoutManager)

    listAdapter = app.makeListAdapter(readings, this)
    recyclerView.setAdapter(listAdapter)
    recyclerView.addOnScrollListener(onScrollListener)

    chart = findViewById(R.id.chart)
    chart.setOnChartValueSelectedListener(this)

    addMoreData(true)
    goToFirstElement()
  }

  override protected def onPause() {
    super.onPause()
    readingDao.persistData()
  }

  private def addMoreData(fullReset: Boolean) {
    val startPos = readings.size
    readings.appendAll(readingDao.getMany(READING_TYPE, startPos, PAGE_COUNT))

    if (fullReset) listAdapter.notifyDataSetChanged()
    else listAdapter.notifyItemRangeInserted(startPos, PAGE_COUNT)

    refillChart()
  }

  private def refillChart() {
    chart.clear()

    val glucoseSet = new LineDataSet(
      createEntries(readings.toList, makeGlucoseXY).asJava,
      getString(R.string.label_glucose))
    styleLineDataSet(glucoseSet,
                     getColor(R.color.darkGreen),
                     getColor(R.color.grey200))

    val ketoneSet = new LineDataSet(
      createEntries(readings.toList, makeKetoneXY).asJava,
      getString(R.string.label_ketone))
    styleLineDataSet(ketoneSet,
                     getColor(R.color.brightGreen),
                     getColor(R.color.darkGreen))

    chart.setData(new LineData(ketoneSet, glucoseSet))
    chart.setVisibleXRange(7 * DAY_IN_SECONDS, 7 * DAY_IN_SECONDS)
  }

  private def goToFirstElement() {
    if (readings.nonEmpty && readings.head.getGlucose.isPresent) {
      val xyPair = makeGlucoseXY(readings.head)
      chart.centerViewTo(xyPair.first, xyPair.second, YAxis.AxisDependency.LEFT)
    }
  }

  private def makeGlucoseXY(reading: Reading): Pair[Float, Float] = {
    val ts = reading.getTimestamp
    Pair.create(ts.floatValue, reading.getGlucose.get)
  }

  private def makeKetoneXY(reading: Reading): Pair[Float, Float] = {
    val ts = reading.getTimestamp
    val value = reading.getGlucose.get + reading.getKetone.get
    Pair.create(ts.floatValue, value)
  }

  override protected def onActivityResult(requestCode: Int,
                                          resultCode: Int,
                                          data: Intent) {
    super.onActivityResult(requestCode, resultCode, data)

    requestCode match {
      case READING_DETAIL_REQUEST =>
        if (resultCode == Activity.RESULT_OK) {
          readings.clear
          addMoreData(true)
        }

    }
  }

  private def createEntries(readings: List[Reading],
                            makeXY: Reading => Pair[Float, Float]) = {
    readings.zipWithIndex
      .filter {
        case (r, idx) => r.getGlucose.isPresent && r.getKetone.isPresent
      }
      .map {
        case (r, idx) =>
          val pair = makeXY(r)
          new Entry(pair.first, pair.second, idx)
      }
      .sortBy(e => e.getX)
  }

  private def isNotVisibleInX(v: Float) =
    v > chart.getHighestVisibleX || v < chart.getLowestVisibleX

  override def onCreateOptionsMenu(menu: Menu) = {
    getMenuInflater.inflate(R.menu.menu_main, menu)
    deleteMenu = menu.findItem(R.id.main_delete)
    true
  }

  override def onOptionsItemSelected(item: MenuItem) = {
    item.getItemId match {

      case R.id.main_delete =>
        val selectedUuid = readings(selectedItem).getId
        readingDao.delete(selectedUuid)

        readings.clear
        listAdapter.selectItem(-1)
        setPosition(-1)
        addMoreData(true)

    }

    true
  }

  private def setPosition(pos: Int) {
    selectedItem = pos

    if (pos == -1) deleteMenu.setVisible(false)
    else deleteMenu.setVisible(true)
  }

  override def onItemClick(view: View, position: Int): Unit = {
    setPosition(position)

    if (position == -1) {
      chart.highlightValue(-1, -1, false)
      return
    }

    val reading = readings(position)
    val xVal = reading.getTimestamp
    chart.highlightValue(xVal, 0, false)

    if (isNotVisibleInX(xVal) && reading.getGlucose.isPresent) {
      chart.centerViewTo(xVal,
                         reading.getGlucose.get,
                         YAxis.AxisDependency.LEFT)
    }
  }

  override def onValueSelected(entry: Entry, h: Highlight) {
    val pos = entry.getData.asInstanceOf[Int]
    listAdapter.selectItem(pos)
    layoutManager.scrollToPositionWithOffset(pos, 0)
    setPosition(pos)
  }

  override def onNothingSelected() {
    listAdapter.selectItem(-1)
    setPosition(-1)
  }

  override def onClick(v: View) {
    v.getId match {
      case R.id.fab =>
        val intent = new Intent(this, classOf[ReadingDetailActivity])
        intent.setAction(MainActivity.ACTION_ADD_READING)
        startActivityForResult(intent, READING_DETAIL_REQUEST)

    }
  }
}

object MainActivity {
  val ACTION_ADD_READING = "ADD_READING"
}
