package in.abaddon.butterfruitlevel

import java.util.{Optional, UUID}

import in.abaddon.butterfruitlevel.models._
import org.scalatest.{Matchers, WordSpec}

import scala.collection.mutable.ListBuffer

class MemoryReadingRepositorySpec extends WordSpec with Matchers {

  "getMany" when {

    "Glucose" should {

      "have correct size" in {
        val dao = new MemoryReadingRepository(null)
        dao.add(
          new Reading(
            UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            None,
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("136e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            None,
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("236e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            None,
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("336e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            None,
            Some(4.0f),
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("436e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            None,
            Some(4.0f),
            "...notes"
          )
        )

        dao.getMany(Glucose, 0, 5).size shouldBe 3
      }

      "be sorted desc by timestamp" in {
        val dao = new MemoryReadingRepository(null)
        val r1 = new Reading(
          UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc"),
          1,
          Some(41.0f),
          Some(4.0f),
          "...notes"
        )
        val r2 = new Reading(
          UUID.fromString("136e315a-4b5f-401b-9f7c-19c7f0351afc"),
          2,
          Some(42.0f),
          None,
          "...notes"
        )
        val r3 = new Reading(
          UUID.fromString("236e315a-4b5f-401b-9f7c-19c7f0351afc"),
          3,
          Some(43.0f),
          None,
          "...notes"
        )
        val r4 = new Reading(
          UUID.fromString("336e315a-4b5f-401b-9f7c-19c7f0351afc"),
          4,
          Some(44.0f),
          Some(4.0f),
          "...notes"
        )
        val r5 = new Reading(
          UUID.fromString("436e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5,
          Some(45.0f),
          Some(4.0f),
          "...notes"
        )

        dao.add(r2)
        dao.add(r4)
        dao.add(r3)
        dao.add(r1)
        dao.add(r5)

        dao.getMany(Glucose, 0, 5) shouldBe List(r5, r4, r3, r2, r1)

      }
    }

    "Ketone" should {
      "have correct size" in {
        val dao = new MemoryReadingRepository(null)
        dao.add(
          new Reading(
            UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            None,
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("136e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            None,
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("236e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            None,
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("336e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            None,
            Some(4.0f),
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("436e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            None,
            Some(4.0f),
            "...notes"
          )
        )

        dao.getMany(Ketone, 0, 5).size shouldBe 2

      }
      "be sorted desc by timestamp" in {
        val dao = new MemoryReadingRepository(null)
        val r1 = new Reading(
          UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc"),
          1,
          Some(41.0f),
          Some(31.0f),
          "...notes"
        )
        val r2 = new Reading(
          UUID.fromString("136e315a-4b5f-401b-9f7c-19c7f0351afc"),
          2,
          Some(42.0f),
          None,
          "...notes"
        )
        val r3 = new Reading(
          UUID.fromString("236e315a-4b5f-401b-9f7c-19c7f0351afc"),
          3,
          Some(43.0f),
          None,
          "...notes"
        )
        val r4 = new Reading(
          UUID.fromString("336e315a-4b5f-401b-9f7c-19c7f0351afc"),
          4,
          None,
          Some(32.0f),
          "...notes"
        )
        val r5 = new Reading(
          UUID.fromString("436e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5,
          None,
          Some(33.0f),
          "...notes"
        )
        dao.add(r2)
        dao.add(r4)
        dao.add(r3)
        dao.add(r1)
        dao.add(r5)

        dao.getMany(Ketone, 0, 5) shouldBe List(r5, r4, r1)
      }
    }
    "GKI" should {
      "have correct size" in {
        val dao = new MemoryReadingRepository(null)
        dao.add(
          new Reading(
            UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            Some(4.0f),
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("136e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            None,
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("236e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            None,
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("336e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            Some(4.0f),
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("436e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            None,
            Some(4.0f),
            "...notes"
          )
        )

        dao.getMany(GKI, 0, 5).size shouldBe 2
      }
      "be sorted desc by timestamp" in {
        val dao = new MemoryReadingRepository(null)
        val r1 = new Reading(
          UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc"),
          1,
          Some(41.0f),
          Some(31.0f),
          "...notes"
        )
        val r2 = new Reading(
          UUID.fromString("136e315a-4b5f-401b-9f7c-19c7f0351afc"),
          2,
          Some(42.0f),
          None,
          "...notes"
        )
        val r3 = new Reading(
          UUID.fromString("236e315a-4b5f-401b-9f7c-19c7f0351afc"),
          3,
          Some(43.0f),
          None,
          "...notes"
        )
        val r4 = new Reading(
          UUID.fromString("336e315a-4b5f-401b-9f7c-19c7f0351afc"),
          4,
          None,
          Some(32.0f),
          "...notes"
        )
        val r5 = new Reading(
          UUID.fromString("436e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5,
          Some(45.0f),
          Some(33.0f),
          "...notes"
        )
        dao.add(r2)
        dao.add(r4)
        dao.add(r3)
        dao.add(r1)
        dao.add(r5)
        dao.getMany(GKI, 0, 5) shouldBe List(r5, r1)
      }
    }
    "AnyReading" should {
      "have correct size" in {
        val dao = new MemoryReadingRepository(null)
        dao.add(
          new Reading(
            UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            Some(4.0f),
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("136e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            None,
            None,
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("236e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            None,
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("336e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            Some(4.0f),
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("436e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            None,
            None,
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("536e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            None,
            Some(4.0f),
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("636e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            None,
            None,
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("736e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            None,
            Some(4.0f),
            "...notes"
          )
        )
        dao.add(
          new Reading(
            UUID.fromString("836e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            None,
            "...notes"
          )
        )

        dao.getMany(AnyReading, 0, 20).size shouldBe 6

      }
    }

    "fetch infinitely" in {
      val dao = new MemoryReadingRepository(null)
      dao.add(
        new Reading(
          UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5559,
          Some(41.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("136e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5558,
          Some(42.0f),
          None,
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("236e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5557,
          Some(43.0f),
          None,
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("336e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5556,
          Some(44.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("436e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5555,
          Some(45.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("536e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5554,
          Some(46.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("636e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5553,
          Some(47.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("736e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5552,
          Some(48.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("836e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5551,
          Some(49.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("936e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5550,
          Some(50.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("a36e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5549,
          Some(51.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("b36e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5548,
          Some(52.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("c36e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5547,
          Some(53.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("d36e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5546,
          Some(54.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("e36e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5545,
          Some(55.0f),
          Some(4.0f),
          "...notes"
        )
      )
      dao.add(
        new Reading(
          UUID.fromString("f36e315a-4b5f-401b-9f7c-19c7f0351afc"),
          5544,
          Some(56.0f),
          Some(4.0f),
          "...notes"
        )
      )
      val actual = ListBuffer[Reading]()

      actual.appendAll(dao.getMany(Glucose, actual.size, 4))
      actual.last shouldBe new Reading(
        UUID.fromString("336e315a-4b5f-401b-9f7c-19c7f0351afc"),
        5556,
        Some(44.0f),
        Some(4.0f),
        "...notes"
      )

      actual.appendAll(dao.getMany(Glucose, actual.size, 4))
      actual.last shouldBe new Reading(
        UUID.fromString("736e315a-4b5f-401b-9f7c-19c7f0351afc"),
        5552,
        Some(48.0f),
        Some(4.0f),
        "...notes"
      )

      actual.appendAll(dao.getMany(Glucose, actual.size, 10))
      actual.last shouldBe new Reading(
        UUID.fromString("f36e315a-4b5f-401b-9f7c-19c7f0351afc"),
        5544,
        Some(56.0f),
        Some(4.0f),
        "...notes"
      )
    }
  }

  "get" when {
    "is empty" should {
      "return None" in {
        val dao = new MemoryReadingRepository(null)
        dao.get(UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc")) shouldBe Optional
          .empty()

      }
    }

    "has data" should {
      "return some" in {
        val dao = new MemoryReadingRepository(null)
        dao.add(
          new Reading(
            UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            Some(4.0f),
            "...notes"
          )
        )
        dao.get(UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc")) shouldBe Optional
          .of(
            new Reading(
              UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc"),
              5555,
              Some(4.0f),
              Some(4.0f),
              "...notes"
            ))

      }
    }
  }

  "add" when {
    "inserting existing id" should {
      "throw exception" in {
        val dao = new MemoryReadingRepository(null)
        dao.add(
          new Reading(
            UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc"),
            5555,
            Some(4.0f),
            Some(4.0f),
            "...notes"
          )
        )
        assertThrows[IllegalArgumentException] {
          dao.add(
            new Reading(
              UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc"),
              5533,
              Some(5.0f),
              Some(3.0f),
              "...notes"
            )
          )
        }
      }
    }
  }

  "update" should {
    "successful" in {
      val dao = new MemoryReadingRepository(null)
      val uuid = UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc")
      dao.add(new Reading(uuid, 5555, Some(4.0f), Some(4.0f), "...notes"))

      dao.update(uuid, new Reading(uuid, 2222, Some(42.0f), None, "...updated")) shouldBe true
      dao.get(uuid) shouldBe Optional.of(
        new Reading(uuid, 2222, Some(42.0f), None, "...updated"))
    }

    "unsuccessful" in {
      val dao = new MemoryReadingRepository(null)
      val uuid = UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc")

      dao.update(uuid, new Reading(uuid, 2222, Some(42.0f), None, "...updated")) shouldBe false
    }
  }

  "delete" should {
    "successful" in {
      val dao = new MemoryReadingRepository(null)
      val uuid = UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc")
      dao.add(new Reading(uuid, 5555, Some(4.0f), Some(4.0f), "...notes"))

      dao.delete(uuid) shouldBe true
      dao.get(uuid) shouldBe Optional.empty
    }

    "unsuccessful" in {
      val dao = new MemoryReadingRepository(null)

      dao.delete(UUID.fromString("036e315a-4b5f-401b-9f7c-19c7f0351afc")) shouldBe false
    }
  }

}
